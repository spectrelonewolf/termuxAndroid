#!/bin/bash

function generar_barras() {
    local procesos=("$@")

    for proceso in "${procesos[@]}"; do
        local pcpu=$(echo "$proceso" | awk '{print $1}')
        local pid=$(echo "$proceso" | awk '{print $2}')
        local command=$(echo "$proceso" | awk '{$1=""; $2=""; $3=""; print $0}')

        local ancho_barras=$(awk "BEGIN {printf \"%.0f\", $pcpu * 2}")

        printf "|"
        for (( i=1; i<=$ancho_barras; i++ )); do
            printf "="
        done
        printf "| %4s%% %s %s\n" "$pcpu" "$command" "$pid"
    done
}

# Ejemplo de datos de procesos (simulados)
procesos=(
    "41.3 77438 spectre+ scrcpy"
    "12.2 82866 spectre+ /usr/share/code/code"
    "9.2 601 root /usr/lib/xorg/Xorg"
    "8.3 82811 spectre+ /usr/share/code/code"
    "7.3 83267 spectre+ /usr/bin/python3"
    "5.4 84695 spectre+ /usr/lib/x86_64-linux-gnu/qt5/libexec/QtWebE"
    "4.5 83523 spectre+ /usr/lib/x86_64-linux-gnu/qt5/libexec/QtWebE"
    "3.1 82719 spectre+ /usr/share/code/code"
)

# Llamada a la función para generar las barras
generar_barras "${procesos[@]}"
