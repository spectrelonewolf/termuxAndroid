#!/bin/bash
#//sudo apt install cmatrix
#/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
#brew intall cmatrix
echo "terminal"
function ventanaGnome2(){
   gnome-terminal --hide-menubar  --title="Sensores" --zoom=0.7 --geometry=47x18 -- bash -c "echo -e '\e]10;rgb:00/ff/ff\a' & echo -e '\e]11;rgb:00/00/00\a' & watch -n 2 sensors; exec bash" 
}

function ventanaGnome3(){
   gnome-terminal --hide-menubar --title="Conexiones Establecidas y Procesos" --zoom=0.8 --geometry=100x15 -- bash -c "echo -e '\e]10;rgb:00/ff/00\a' & echo -e '\e]11;rgb:00/00/00\a' & netstat -nctup ;exec bash" 
}

function ventanaGnome4(){
   gnome-terminal --hide-menubar --title="Carga y Procesos" --zoom=0.8 --geometry=80x17 -- bash -c "echo -e '\e]10;rgb:00/ff/ff\a' & echo -e '\e]11;rgb:00/00/00\a' & top;exec bash"
}

function ventanaGnome5(){
   gnome-terminal --hide-menubar --title="Packetes y Conexiones" --zoom=0.7 --geometry=70x20 -- bash -c "echo -e '\e]10;rgb:00/ff/00\a' & echo -e '\e]11;rgb:00/00/00\a' & sudo tcpdump -n -vvv | grep --color=auto -E -w -i 'id|proto|>' | cut -d ' ' -f 7,8,13,14,15,1,2,3,4,5,6
;exec bash"
}

function ventanaGnome6(){
   gnome-terminal --hide-menubar --title="Farandula" --zoom=0.7 --geometry=93x15 -- bash -c "echo -e '\e]10;rgb:ff/00/ff\a' & echo -e '\e]11;rgb:00/00/00\a' & cmatrix -C red -s ;exec bash"
}

function ventanaGnome7(){
   gnome-terminal --hide-menubar --title="Saltos de la conexion" --zoom=0.7 --geometry=70x15 -- bash -c '
   function saltosConexion(){
   	clear
   	echo "Ingresa ip para comprobar saltos: "
   	read hostname
	traceroute -n "$hostname"
	read espera
	echo "presione ENTER para continuar"
	saltosConexion
   }
   echo -e "\e]10;rgb:ff/00/ff\a" & echo -e "\e]11;rgb:00/00/00\a" & saltosConexion ;exec bash'
}
function ventanaGnome8(){
   gnome-terminal --hide-menubar --title="Saltos de la conexion" --zoom=0.7 --geometry=70x15 -- bash -c '
   function saltosConexion(){
   	clear
   	echo "Ingresa ip para comprobar saltos: "
   	read hostname
	traceroute -n "$hostname"
	read espera
	echo "presione ENTER para continuar"
	saltosConexion
   }
   echo -e "\e]10;rgb:ff/00/ff\a" & echo -e "\e]11;rgb:00/00/00\a" & saltosConexion ;exec bash'
}


ventanaGnome2
ventanaGnome3
ventanaGnome4
ventanaGnome5
ventanaGnome6
ventanaGnome7
